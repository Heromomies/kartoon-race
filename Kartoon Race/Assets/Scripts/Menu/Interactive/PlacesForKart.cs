﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlacesForKart : MonoBehaviour
{
    [Header("Value")] 
    public int numberCube;

    [Header("HUD")]
    public TextMeshProUGUI textPlayer;
    public Slider sliderGarageBar;

    private int _forceMultiple = 1;
    private bool _onExit = true;
    private float _timeToWait = 2f;
    private float _sliderForce = 0f;
    private float _sliderMaxForce = 1f;

    public void Start()
    {
        sliderGarageBar.value = _sliderForce;
    }

    private void Update()
    {
        if (_timeToWait <= 0)
        {
            TimeToSetText();
        }
        if (_timeToWait <= -2)
        {
            _timeToWait = -1;
        }
        if (_timeToWait == 1)
        {
            textPlayer.text = "";
        }

        if (_onExit) //On baisse le slider 
        {
            _sliderForce += -0.005f * _forceMultiple;
            sliderGarageBar.value = _sliderForce;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _timeToWait -= Time.deltaTime;
            _sliderForce = sliderGarageBar.value;
            _forceMultiple = 1;
            _onExit = false;
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _onExit = false;
            _timeToWait -= Time.deltaTime;

            //On augmente le slider 
            _sliderForce += 0.01f * _forceMultiple;
            sliderGarageBar.value = _sliderForce;
            if (_sliderForce >= 1 || _sliderForce <= 0)
            {
                _sliderForce = _sliderMaxForce;
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _timeToWait = 2f;
            _onExit = true;
            StartCoroutine(TimeToResetText());
        }
    }

    public void TimeToSetText()
    {
        textPlayer.text = "Joueur n°" + numberCube;
        Debug.Log("Cube n° " + numberCube);
    }
    
    IEnumerator TimeToResetText()
    {
        yield return new WaitForSeconds(0.1f);
        textPlayer.text = "";
    }
}
