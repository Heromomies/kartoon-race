﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void Quit()
    {
        Application.Quit();
    }
    
    public void MenuStart()
    {
        SceneManager.LoadScene("Interactive Menu");
    }

    public void LoadOptions()
    {
        SceneManager.LoadScene("Options");
    }
}
